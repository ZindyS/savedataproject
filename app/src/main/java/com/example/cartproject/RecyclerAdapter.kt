package com.example.cartproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cartproject.databinding.ActivityMainBinding
import com.example.cartproject.databinding.ItemBinding

class RecyclerAdapter(val list : List<RecItem>) : RecyclerView.Adapter<RecyclerAdapter.VH>() {
    class VH(val binding: ItemBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(ItemBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.binding.textView.text = list[position].str
        holder.binding.back.setOnClickListener {
            clickedItem = list[position]
            MainActivity.startAct()
        }
    }
}