package com.example.cartproject

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activity = this

        val list = listOf(RecItem("1"), RecItem("sec"), RecItem("3"), RecItem("third"),
            RecItem("5"), RecItem("19"))
        findViewById<RecyclerView>(R.id.recyclerView).adapter = RecyclerAdapter(list)
    }

    companion object {
        lateinit var activity : Activity
        fun startAct() {
            activity.startActivity(Intent(activity, CourseActivity::class.java))
        }
    }
}