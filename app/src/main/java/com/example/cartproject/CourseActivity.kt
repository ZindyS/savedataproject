package com.example.cartproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

lateinit var clickedItem : RecItem
class CourseActivity : AppCompatActivity() {
    var cnt = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course)
        cnt = 0
        findViewById<TextView>(R.id.textView2).text = clickedItem.str
    }

    fun onButtonClicked(v: View) {
        val sh = getSharedPreferences("Codemania", 0)
        val ed = sh.edit()
        if (cnt==0) {
            findViewById<Button>(R.id.button).text = "Перейти в корзину"
            var sz = sh.getInt("sz", -1)
            sz++
            ed.putInt("sz", sz)
            ed.putString("name$sz", clickedItem.str)
            ed.commit()
        } else {
            startActivity(Intent(this, CartActivity::class.java))
        }
        cnt++
    }
}